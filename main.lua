local socket = require("socket")
local conn = socket.tcp()
local turn = false
local go = false
local info = {}
local timer1 = 0
local goax = 0
local touch_joystick = {
	x=200, y=200, r=50, 
	sx = 0, sy = 0, sr = 25
}


function love.load()
	conn:connect("192.168.1.147", 1508)
end

function love.draw()
	for key,str in pairs(info) do
		love.graphics.print(str, 10, key*12)
	end
	
	love.graphics.circle( "line", touch_joystick.x, touch_joystick.y, touch_joystick.r )
	love.graphics.circle( "fill", touch_joystick.x + touch_joystick.sx, touch_joystick.y + touch_joystick.sy, touch_joystick.sr )
end

function love.update(dt)
	if #info >= 5 then timer1 = timer1 + dt end
	if timer1 >= 1 then 
		timer1 = 0
		table.remove(info, 1)
	end
	
	dir = touch_joystick.sy /50
	
	if goax ~= 0 and dir == 0 then conn:send("2\0001\000") goax = dir end
	
	if math.abs(math.abs(goax) - math.abs(dir)) > 0.05 then
		if dir > 0 then
			if goax < 0 then conn:send "1\000" end
			conn:send ("2"..string.char(255*math.abs(dir)))
		elseif dir < 0 then
			if goax > 0 then conn:send "2\000"  end
			conn:send ("1"..string.char(255*math.abs(dir)))
		elseif dir == 0 then conn:send "2\0001\000"
		end
		goax = dir
	end
	
	if math.abs(touch_joystick.sx) > 10 then
		if touch_joystick.sx < 0 then
			if turn == "d" then 
				conn:send "6" 
			elseif turn ~= "a" then
				conn:send "3"
				turn = "a"
			end
		else
			if turn == "a" then 
				conn:send "6"
			elseif turn ~= "d" then
				conn:send "4"
				turn = "d"
			end
		end
	elseif turn ~= false then
		conn:send "6"
		turn = false
	end
end

function love.keypressed( key, scancode, isrepeat )
	if key == "w" then
		if go ~= key then conn:send("2\000") end
		go = key
		conn:send("1\200")
	elseif key == "a" then
		if turn ~= key then conn:send("6") end
		turn = key
		conn:send("3")
	elseif key == "s" then
		if go ~= key then conn:send("2\000") end
		go = key
		conn:send("2\200")
	elseif key == "d" then
		if turn ~= key then conn:send("6") end
		turn = key
		conn:send("4")
	end
end

function love.keyreleased( key, scancode )
	if key == "w" then
		conn:send("1\000")
	elseif key == "a" then
		conn:send("6")
	elseif key == "s" then
		conn:send("2\000")
	elseif key == "d" then
		conn:send("6")
	end
end

--[[function love.touchpressed( id, x, y, dx, dy, pressure )
	table.insert(info, "x = "..x.." y = "..y.." dx = "..dx.." dy = "..dy)
end]]

function love.touchmoved( id, x, y, dx, dy, pressure )
	if ((touch_joystick.x - x)^2 + (touch_joystick.y - y)^2)^0.5 <= touch_joystick.r then
		touch_joystick.sx = x - touch_joystick.x
		touch_joystick.sy = y - touch_joystick.y
		--table.insert(info, touch_joystick.sx)
	end
	
	--table.insert(info, "x = "..x.." y = "..y.." dx = "..dx.." dy = "..dy)
end

function love.touchreleased( id, x, y, dx, dy, pressure )
	if ((touch_joystick.x - x)^2 + (touch_joystick.y - y)^2)^0.5 <= touch_joystick.r then
		touch_joystick.sx = 0
		touch_joystick.sy = 0
	end
end
